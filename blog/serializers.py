﻿from rest_framework import serializers
from blog.models import Client, Lunette, Commande

class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields='__all__'

class LunetteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Lunette
        fields='__all__'

class CommandeSerializer(serializers.ModelSerializer):
    client = ClientSerializer()
    lunette = LunetteSerializer()
    class Meta:
        model = Commande
        fields='__all__'



