﻿from django.urls import path
from django.conf.urls import url
from rest_framework_jwt.views import obtain_jwt_token, refresh_jwt_token, verify_jwt_token

from . import views
from rest_framework import routers

router =routers.DefaultRouter()

urlpatterns = [
    path('',views.index, name='index'),
    url(r'^clients/$',views.ClientListCreateView.as_view(), name='clients'),
    url(r'^clientupdate/(?P<pk>[0-9]+)/$',views.ClientUpdateView.as_view()),
    url(r'^clientdelete/(?P<pk>[0-9]+)/$',views.ClientDeleteView.as_view()),
    url(r'^lunettes/$',views.LunetteListCreateView.as_view(), name='lunettes'),
    url(r'^lunetteupdate/(?P<pk>[0-9]+)/$',views.LunetteUpdateView.as_view()),
    url(r'^lunettedelete/(?P<pk>[0-9]+)/$',views.LunetteDeleteView.as_view()),
    url(r'^commandes/$',views.CommandeListCreateView.as_view(), name='commandes'),
    url(r'^commandeupdate/(?P<pk>[0-9]+)/$',views.CommandeUpdateView.as_view()),
    url(r'^commandedelete/(?P<pk>[0-9]+)/$',views.CommandeDeleteView.as_view()),


    url(r'^login/$', obtain_jwt_token),
]
