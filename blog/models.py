from django.db import models

# Create your models here.

class Client (models.Model):
    nom = models.CharField(max_length=30, blank=False, null=False)
    prenom = models.TextField(max_length=100, blank=False, null=False)
    adresse= models.TextField(max_length=1000, blank=False, null=False)
    telephone= models.CharField(max_length=100, blank=False, null=False)
    photo= models.FileField(blank=True, null=True)

    def __str__(self):
       return str(self.nom)+' '+str(self.prenom)+' '+str(self.adresse)+' '+str(self.telephone)

class Lunette (models.Model):
    nom = models.CharField(max_length=30, blank=False, null=False)
    typeLunette = models.CharField(max_length=100, blank=False, null=False)
    prix= models.CharField(max_length=100, blank=False, null=False)
    photo= models.FileField(blank=True, null=True)
    def __str__(self):
        return str(self.nom)+'\n Type:  '+str(self.typeLunette)+'\n prix:  '+str(self.prix)+'\n photo: '+str(self.photo)

class Commande (models.Model):
    date = models.DateField(max_length=30, blank=False, null=False)
    nbre_lunettes = models.CharField(max_length=1000, blank=False, null=False)
    montant_total= models.CharField(max_length=100,blank=True, null=True)
    client= models.ForeignKey('Client', on_delete=models.CASCADE, null=True, blank=True);
    lunette= models.ForeignKey('Lunette', on_delete=models.CASCADE, null=True, blank=True);

    def __str__(self):
        return 'Date : '+str(self.date)+' \n nombre de lunettes:  '+str(self.nbre_lunettes)+'\n montant total :  '+str(self.montant_total)+'\n montant total : '+'\n Client:'+str(self.client)+'\n Lunette: '+str(self.lunette.nom)